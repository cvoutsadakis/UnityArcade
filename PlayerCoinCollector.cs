﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerCoinCollector : MonoBehaviour {

	public Text coinText;

	private AudioSource audioSource;
	private int coinCount;

	void Awake ()
	{
		audioSource = GetComponent <AudioSource>();
		coinCount = 0;
		SetCoinCountText();
	}

	void OnTriggerEnter(Collider other)
    {
    	if(other.tag == "Coin")
    	{
    		coinCount++;
    		SetCoinCountText();
    		audioSource.Play();
    	}
    }

    void SetCoinCountText()
    {
    	if(coinCount == 1) { coinText.text = "1 coin"; }
    	else { coinText.text = coinCount.ToString() + " coins"; }
    }
}
