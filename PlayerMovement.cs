﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float speed;
	public float rotationSpeed;
	public float runningJumpHeight;
	public float idleJumpHeight;
	//public float crouchingSpeed;

	private Vector3 movement;
	private Vector3 jumpingTo;
	private Rigidbody rb;
	private Animator anim;
	private bool jump = false;
	private bool falling = false;
	//bool running = false;
	//bool crouching = false;

	void Awake ()
	{
		rb = GetComponent<Rigidbody>();
		anim = GetComponent <Animator> ();
	}

	void FixedUpdate ()
	{
		float h = Input.GetAxisRaw("Horizontal");
		Rotate(h);

		float v = Input.GetAxisRaw("Vertical");

		jump = Input.GetKeyDown("space");
		
		if(v!=0 && !this.anim.GetCurrentAnimatorStateInfo(0).IsName("idle jump")) // the second condition doesn't let the player move while idle jumping
		{
			Move(v);
		}

		if(jump == true && transform.position.y < 0.52f)
		{
			Jump(v);
		}

		if(transform.position.y < 0.35f)
		{
			falling = true;
		}

		Animating(v);
	}

	void Move (float v)
	{
		movement.Set(0f, 0f, v);

		movement = rb.transform.rotation * Vector3.forward;
		
		// if(crouching == true)
		// {
		// 	movement = movement * crouchingSpeed * Time.deltaTime;
		// }
		// else if(running == true)
		// {
		// 	movement = movement * runningSpeed * Time.deltaTime;
		// }
		// else 
		// {
			movement = movement * speed * Time.deltaTime;
		//}
		
		if(v<0)
		{
			movement = - movement;
		}

		rb.MovePosition(transform.position + movement);
	}

	void Animating (float v)
    {
        bool running = v != 0f;

        //if the running jump animation is playing, don't go back to the running animation unti it has finished
     	if(this.anim.GetCurrentAnimatorStateInfo(0).IsName("running jump")) 
     	{
     		running = false;
     	}
 		anim.SetBool("running", running);
  
        // crouching = Input.GetKey("c");
        // anim.SetBool("IsCrouching", crouching);

 		if(falling == true)
 		{
 			anim.SetBool("falling", falling);
 		}

        if(jump==true)
        {
        	anim.SetTrigger("jumping");
        }       
    }

    void Rotate(float h)
    {	
    	rotationSpeed = h * 2f;    	
		transform.Rotate(0, rotationSpeed, 0, Space.Self);	 
    }

    void Jump (float v)
    {  
    	float jumpingHeight;

    	if(v == 0)
    	{
    		jumpingHeight = idleJumpHeight;
    	}
    	else
    	{
    		jumpingHeight = runningJumpHeight;
    	}

    	GetComponent <Rigidbody>().AddForce (Vector3.up * jumpingHeight);
    }
}
