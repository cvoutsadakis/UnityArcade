using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleController : MonoBehaviour {

	private Animator anim;
	
 	void Update() 
    {
        transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other) 
    {   
    	other.GetComponent <PlayerMovement>().enabled = false;
    	anim = other.GetComponent <Animator>();
    	anim.SetBool("running", false);
		this.gameObject.SetActive (false);
    }
}