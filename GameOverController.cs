﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameOverController : MonoBehaviour {
	
	public GameObject player;
	public GameObject end;
	public float restartDelay;
	public Text gameOverText;

	private float restartTimer;
	private AudioSource[] audioSource;
	private AudioSource gameOverSuccess;
	private AudioSource gameOverFailure;
	private Animator gameoverAnim;
	private Text endText;
	//private Color successColor;

	void Start()
	{
		audioSource = GetComponents <AudioSource> ();
		gameOverSuccess = audioSource[0];
		gameOverFailure = audioSource[1];

		gameoverAnim = gameOverText.GetComponent <Animator>();
		endText = gameOverText.GetComponent <Text>();

		//successColor = new Color(0f, 1f, 0f, 1f);
	}

	void Update () 
	{
		if((player.transform.GetChild(1).gameObject.activeSelf == false)||(end.activeSelf == false))
		{
			GameOver();
		}	
	}

	void GameOver()
	{
		if(end.activeSelf == false)
		{
			//endText.color =  new Color(0f, 1f, 0f, 1f);
			endText.text = "Level Complete!";
			gameoverAnim.SetTrigger("LevelComplete");
			gameOverSuccess.enabled = true;
		}
		else if(player.transform.GetChild(1).gameObject.activeSelf == false)
		{
			gameOverFailure.enabled = true;
			gameoverAnim.SetTrigger("GameOver");
		}

		
		Restart();
	}

	void Restart()
	{
		 restartTimer += Time.deltaTime;

        if(restartTimer >= restartDelay)
        {
       		SceneManager.LoadScene("Scene01", LoadSceneMode.Single);	
        }
	}
}
